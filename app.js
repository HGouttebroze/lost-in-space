console.log("test affichage");

/*********************************************************************************************************************
 *************************************** CONFIGURATION DU JEU ************************************************
 *********************************************************************************************************************/
const config = {
  width: 500,
  height: 300,
  type: Phaser.AUTO,
  physics: {
    default: "arcade", // add gravity to img
    arcade: {
      gravity: { y: 450 },
    },
  },
  scene: {
    preload: preload,
    create: create,
    update: update,
  },
};

/* initialisation d'un nouveau jeu (un objet) phaser nommé 'game'*/
/*********************************************************************************************************************
 *************************************** INITIALISATION DU JEU ************************************************
 *********************************************************************************************************************/
/*
 * initialisation d'un nouveau jeu (un objet) phaser 'game'
 * l objet 'game' prend 3 parametres: largeur à 500 px, hauteur à 300,
 * 3eme: type de rendu à utiliser pr générer le jeu: soit avec canevas en 2d, soit avec webql, ici methode AUTO qui trouve le plus optimisé (ici webgl)
 */
var game = new Phaser.Game(config);

let smiley;
let cursors;

/*********************************************************************************************************************
 *************************************** INITIALISATION DES FONCTIONS ************************************************
 *********************************************************************************************************************/
/*
 * x: axe horizontal
 * y: axe vertical
 */
function preload() {
  this.load.image("smiley", "imgPhaser/smiley.png");
}

function create() {
  console.log(this);
  smiley = this.physics.add.image(100, 100, "smiley"); // x to 100, y to 100
  smiley.body.collideWorldBounds = true;
  cursors = this.input.keyboard.createCursorKeys();
  console.log(cursors);
}

function update() {
  smiley.setVelocityX(0);

  if (cursors.up.isDown) {
    // console.log("touche appuyée");
    smiley.setVelocity(0, -300); // ajout velocité au click touche haut sur axe vertical
  }
  if (cursors.right.isDown) {
    smiley.setVelocity(300, 0);
  }
  if (cursors.left.isDown) {
    smiley.setVelocity(-300, 0);
  }
}
